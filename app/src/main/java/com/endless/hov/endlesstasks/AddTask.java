package com.endless.hov.endlesstasks;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


public class AddTask extends ActionBarActivity {

    int from;

    EditText etTitle, etDetail, etDuration;
    TextView tvMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_task);

        from = getIntent().getExtras().getInt("ID");


        etTitle = (EditText)findViewById(R.id.etTitle);
        etDetail = (EditText)findViewById(R.id.etDetail);
        etDuration = (EditText)findViewById(R.id.etDuration);
        tvMain = (TextView)findViewById(R.id.tvMain);

        if(from!=0){
            tvMain.setText("This will be a subtask of the task number: " + from);
        }

    }

    public void addTask(){
        final TaskDatabaseHandler db = new TaskDatabaseHandler(getBaseContext());
        String title = etTitle.getText().toString();
        String detail = etDetail.getText().toString();
        int duration = -1;
        try{
            duration = Integer.parseInt(etDuration.getText().toString());
        }catch (Exception e){
            Log.d("Duration", "couldn't get");
        }
        if(title.equals("")){
            Toast.makeText(getBaseContext(), "Title can't be empty!", Toast.LENGTH_LONG).show();
        }
        else if(duration<0){
            Toast.makeText(getBaseContext(), "Enter acceptable value to duraion!", Toast.LENGTH_LONG).show();
        }
        else{
            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy-HH.mm.ss");
            String current = sdf.format(new Date());
            Task task = new Task(from, title, detail, current, duration);
            db.addTask(task);

            if(from!=0) {
                Task back = db.getTask(from);
                if (back.getCheckedBool()) {
                    back.setChecked(false);
                    db.updateTask(back);
                }
            }
        }
    }

    public void updateDuration(int id){

        TaskDatabaseHandler db = new TaskDatabaseHandler(getBaseContext());

        Task task = db.getTask(id);
        List<Task> subs = db.getTasksFrom(id);
        int totalSubs= 0;
        for(Task t : subs){
            totalSubs += t.getDuration();
        }
        task.setDuration(totalSubs);
        db.updateTask(task);
        if(task.getFrom()!=0)
            updateDuration(task.getFrom());
        else finish();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_task, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        switch (id){
            case R.id.action_done:
                addTask();
                if(from!=0)
                    updateDuration(from);
                else finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
