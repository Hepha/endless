package com.endless.hov.endlesstasks;

import android.content.ClipData;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Barış on 17.6.2015.
 */
public class TaskAdapter extends ArrayAdapter<Task> {
    public TaskAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    public TaskAdapter(Context context, int resource, List<Task> tasks) {
        super(context, resource, tasks);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        View v = convertView;

        if(v==null){
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.tasklistrow, null);

        }

        Task t = getItem(position);

        if(t!=null)
        {
            TextView tt1 = (TextView)v.findViewById(R.id.finished);
            TextView tt2 = (TextView)v.findViewById(R.id.title);
            TextView tt3 = (TextView)v.findViewById(R.id.detail);
            TextView tt4 = (TextView)v.findViewById(R.id.duration);

            if(tt4!=null)
                tt4.setText(t.getDuration()+ "min");
            if(tt2!=null)
                tt2.setText(t.getTitle());
            if(tt3!=null) {
                int length = 53 - tt4.getText().length();

                if(t.getDetail().length()<=length)
                    tt3.setText(t.getDetail());
                else
                    tt3.setText(t.getDetail().substring(0,length)+"...");
            }
            if(tt1!=null){
                if(t.getCheckedBool())
                    tt1.setText("Finished");
                else
                    tt1.setText("Not Finished");
            }
        }
        return v;
    }
}
