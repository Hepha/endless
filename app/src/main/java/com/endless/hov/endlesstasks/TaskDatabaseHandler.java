package com.endless.hov.endlesstasks;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;


public class TaskDatabaseHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "tasksManager";
    private static final String TABLE_TASKS = "tasks";

    private static final String KEY_ID = "id";
    private static final String KEY_FROM = "fro";
    private static final String KEY_TITLE = "title";
    private static final String KEY_DETAIL = "detail";
    private static final String KEY_CHECKED = "checked";
    private static final String KEY_ADDTIME = "addTime";
    private static final String KEY_DURATION = "duration";



    public TaskDatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TASKS_TABLE = "CREATE TABLE " + TABLE_TASKS + " ( " + KEY_ID + " INTEGER PRIMARY KEY, " +
                KEY_FROM + " INTEGER, " + KEY_TITLE + " TEXT, " + KEY_DETAIL + " TEXT, " + KEY_CHECKED + " INTEGER, " + KEY_ADDTIME + " TEXT, " +
                KEY_DURATION + " INTEGER" +  ")";
        db.execSQL(CREATE_TASKS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TASKS);
        onCreate(db);
    }

    public void addTask(Task task){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(KEY_FROM, task.getFrom());
        values.put(KEY_TITLE, task.getTitle());
        values.put(KEY_DETAIL, task.getDetail());
        values.put(KEY_CHECKED, task.getChecked());
        values.put(KEY_ADDTIME, task.getAddTime());
        values.put(KEY_DURATION, task.getDuration());

        db.insert(TABLE_TASKS, null, values);
        db.close();
    }

    public Task getTask(int id){
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_TASKS, new String[]{KEY_ID, KEY_FROM, KEY_TITLE, KEY_DETAIL, KEY_CHECKED, KEY_ADDTIME, KEY_DURATION},
                KEY_ID + "=?", new String[]{String.valueOf(id)}, null, null, null, null);

        if(cursor != null) cursor.moveToFirst();

        Task task = new Task(Integer.parseInt(cursor.getString(0)),
                Integer.parseInt(cursor.getString(1)),
                cursor.getString(2),
                cursor.getString(3),
                Integer.parseInt(cursor.getString(4)),
                cursor.getString(5),
                Integer.parseInt(cursor.getString(6)));
        return task;
    }

    public List<Task> getTasksFrom(int from){
        ArrayList<Task> list = new ArrayList<Task>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_TASKS, new String[]{KEY_ID, KEY_FROM, KEY_TITLE, KEY_DETAIL, KEY_CHECKED, KEY_ADDTIME, KEY_DURATION},
                KEY_FROM + "=?", new String[]{String.valueOf(from)},null,null,null,null);
        if(cursor.moveToFirst()){
            do {
                Task task = new Task(Integer.parseInt(cursor.getString(0)),
                        Integer.parseInt(cursor.getString(1)),
                        cursor.getString(2),
                        cursor.getString(3),
                        Integer.parseInt(cursor.getString(4)),
                        cursor.getString(5),
                        Integer.parseInt(cursor.getString(6)));
                list.add(task);
            }while (cursor.moveToNext());
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }

        return list;
    }

    public void deleteTask(Task task){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_TASKS, KEY_ID + "=?", new String[]{String.valueOf(task.getId())});
        db.close();
    }
    public void deleteTask(int id){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_TASKS, KEY_ID + "=?", new String[]{String.valueOf(id)});
        db.close();
    }

    public int updateTask(Task task){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_FROM, task.getFrom());
        values.put(KEY_TITLE, task.getTitle());
        values.put(KEY_DETAIL, task.getDetail());
        values.put(KEY_CHECKED, task.getChecked());
        values.put(KEY_ADDTIME, task.getAddTime());
        values.put(KEY_DURATION, task.getDuration());


        return db.update(TABLE_TASKS, values, KEY_ID + " = ?", new String[]{String.valueOf(task.getId())});
    }
}
