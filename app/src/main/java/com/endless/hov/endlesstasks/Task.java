package com.endless.hov.endlesstasks;

/**
 * Created by Barış on 10.6.2015.
 */
public class Task {
    protected int id, from, checked, duration;
    protected String title, detail, addTime;

    public Task(int id, int from, String title, String detail, int checked, String addTime, int duration) {
        this.id = id;
        this.from = from;
        this.checked = checked;
        this.title = title;
        this.detail = detail;
        this.addTime = addTime;
        this.duration = duration;
    }

    public Task(int from, String title, String detail, String addTime, int duration) {
        this.from = from;
        this.title = title;
        this.detail = detail;
        this.addTime = addTime;
        this.duration = duration;
    }

    @Override
    public String toString(){
        String rtn = getId() + "-" +  getTitle() + " - ";
        if(getCheckedBool()) rtn += "Finished";
        else rtn += "Not Finished";
        return rtn;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public int getChecked() {
        return checked;
    }

    public boolean getCheckedBool(){
        boolean rtn = false;
        if(this.checked==1) rtn = true;
        return rtn;
    }

    public void setChecked(int checked) {
        this.checked = checked;
    }
    public void setChecked(boolean b) {
        if(b) checked = 1;
        else checked = 0;
    }
    public void setChecked() {
        this.checked = 1;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }
}
