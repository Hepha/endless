package com.endless.hov.endlesstasks;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Application;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.xml.datatype.Duration;


public class MainActivity extends ActionBarActivity {


    ListView lvMain;
    TextView tvTitle, tvDetail, tvAddTime, tvDuration;


    List<Task> loadedTasks;
    Task selectedTask;
    int selectedTaskId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        getSupportActionBar().setDisplayHomeAsUpEnabled(false   );

        final TaskDatabaseHandler db = new TaskDatabaseHandler(this);

        lvMain = (ListView)findViewById(R.id.lvMain);
        tvTitle = (TextView)findViewById(R.id.tvTitle);
        tvDetail = (TextView)findViewById(R.id.tvDetail);
        tvAddTime =(TextView)findViewById(R.id.tvAddTime);
        tvDuration = (TextView)findViewById(R.id.tvDuration);

        tvAddTime.setVisibility(View.INVISIBLE);
        tvDuration.setVisibility(View.INVISIBLE);



        selectedTaskId = 0;

        lvMain.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectedTask = loadedTasks.get(position);
                selectedTaskId = selectedTask.getId();
                tvTitle.setText(selectedTask.getTitle());
                tvDetail.setText(selectedTask.getDetail());
                setList(selectedTaskId);
            }
        });

        setList(0);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        setList(selectedTaskId);
    }

    public void setList(int from){
        TaskDatabaseHandler db = new TaskDatabaseHandler(getBaseContext());
        List<Task> list = db.getTasksFrom(from);
        /*String[] arr = new String[list.size()];
        for(int i=0; i<list.size(); i++){
            arr[i] = list.get(i).toString();
        }
        lvMain.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, arr));*/
        TaskAdapter ad = new TaskAdapter(this, R.layout.tasklistrow, list);
        lvMain.setAdapter(ad);
        loadedTasks = list;
        registerForContextMenu(lvMain);
        invalidateOptionsMenu();

        if (from == 0) {
            tvTitle.setText("Your Main Tasks");
            tvDetail.setText("To see their details or subtasks, select a task!");
            selectedTaskId = 0;
            tvAddTime.setVisibility(View.INVISIBLE);
            tvDuration.setVisibility(View.INVISIBLE);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        } else {
            Task backTask = db.getTask(from);
            tvTitle.setText(backTask.getTitle());
            tvDetail.setText(backTask.getDetail());
            tvAddTime.setVisibility(View.VISIBLE);
            tvAddTime.setText("Added Time:" + backTask.getAddTime());
            tvDuration.setVisibility(View.VISIBLE);
            tvDuration.setText("Duration:" + backTask.getDuration() + "min");
            selectedTask = backTask;
            selectedTaskId = backTask.getId();
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }


    }

    public void finishTask(int id, final boolean b, boolean firstTime){

        final TaskDatabaseHandler db= new TaskDatabaseHandler(getBaseContext());
        Task mainTask = db.getTask(id);
        mainTask.setChecked(b);
        db.updateTask(mainTask);
        final List<Task> list = db.getTasksFrom(id);


        if(!b&&firstTime&&(list.size()!=0)){
            new AlertDialog.Builder(this).setTitle("Unfinish All")
                    .setMessage("Do you want to unfinish all the subtask or leave them as finished?")
                    .setPositiveButton("Unfinish All", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if((list.size() != 0)){
                                for(Task t : list){
                                    //fucking recursion baby
                                    finishTask(t.getId(), b, false);
                                }
                            }
                        }
                    })
                    .setNegativeButton("Leave Them", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //Do nothing
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }
        else{
            if((list.size() != 0)){
                for(Task t : list){
                    //fucking recursion baby
                    finishTask(t.getId(), b, false);
                }
            }
        }
        setList(selectedTaskId);

    }

    public void deleteTask(int id, boolean firstTime){
        final TaskDatabaseHandler db = new TaskDatabaseHandler(getBaseContext());
        final Task mainTask = db.getTask(id);
        final List<Task> list = db.getTasksFrom(id);

        if(firstTime){
            new AlertDialog.Builder(this).setTitle("Delete Task")
                    .setMessage("Do you want delete the " + id + ". task and its subtasks?")
                    .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if(list.size() != 0){
                                for(Task t : list){
                                    //fucking recursion baby
                                    deleteTask(t.getId(), false);
                                }
                            }
                            db.deleteTask(mainTask);
                            if(mainTask.getFrom()!=0)
                                updateDuration(mainTask.getFrom());

                            setList(mainTask.getFrom());
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //Do nothing
                        }
                    }).setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }
        else {
            if (list.size() != 0) {
                for (Task t : list) {
                    //fucking recursion baby
                    deleteTask(t.getId(), false);
                }
            }
            Log.d("Deleted task: ", mainTask.getTitle());
            db.deleteTask(mainTask);
        }
    }


    public void checkAllSubs(final int id){
        final TaskDatabaseHandler db = new TaskDatabaseHandler(getBaseContext());
        List<Task> list = db.getTasksFrom(id);
        boolean check = true;
        for(Task t : list){
            if(!t.getCheckedBool())
                check = false;
        }
        if(check&&(id!=0)){
            new AlertDialog.Builder(this).setTitle("All Subs Finished")
                    .setMessage("With this, all the subtasks of " + id + ". task is finished. Do you want set " + id + ". task finished as well?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Task task = db.getTask(id);
                            task.setChecked(true);
                            db.updateTask(task);
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //D nothing...
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }
        setList(selectedTaskId);
    }

    public void changeTitle(int id){
        final TaskDatabaseHandler db = new TaskDatabaseHandler(getBaseContext());
        final Task task = db.getTask(id);

        LayoutInflater li = LayoutInflater.from(this);
        View prompt = li.inflate(R.layout.prompt, null);

        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setView(prompt);

        final EditText et = (EditText)prompt.findViewById(R.id.etAlert);

        et.setText(task.getTitle());

        alert.setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (!et.getText().toString().equals("")) {
                            task.setTitle(et.getText().toString());
                            db.updateTask(task);
                            setList(selectedTaskId);
                        } else {
                            Toast toast = Toast.makeText(getBaseContext(), "Title can't be empty!", Toast.LENGTH_SHORT);
                            toast.show();
                        }
                    }
                })
                .setNegativeButton("Cancel" , new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alert.create();
        alertDialog.show();

    }

    public void changeDetail(int id){
        final TaskDatabaseHandler db = new TaskDatabaseHandler(getBaseContext());
        final Task task = db.getTask(id);

        LayoutInflater li = LayoutInflater.from(this);
        View prompt = li.inflate(R.layout.prompt, null);

        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setView(prompt);

        final TextView tv = (TextView)prompt.findViewById(R.id.tvAlert);
        final EditText et = (EditText)prompt.findViewById(R.id.etAlert);

        tv.setText("Change detail:");
        et.setText(task.getDetail());

        alert.setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        task.setDetail(et.getText().toString());
                        db.updateTask(task);
                        setList(selectedTaskId);

                    }
                })
                .setNegativeButton("Cancel" , new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alert.create();
        alertDialog.show();

    }

    public void changeDuration(int id){
        final TaskDatabaseHandler db = new TaskDatabaseHandler(getBaseContext());
        final Task task = db.getTask(id);

        LayoutInflater li = LayoutInflater.from(this);
        View prompt = li.inflate(R.layout.prompt, null);

        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setView(prompt);

        final TextView tv = (TextView)prompt.findViewById(R.id.tvAlert);
        final EditText et = (EditText)prompt.findViewById(R.id.etAlert);

        tv.setText("Change duration:");
        et.setInputType(InputType.TYPE_CLASS_NUMBER);
        et.setText(""+task.getDuration());

        alert.setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        task.setDuration(Integer.parseInt(et.getText().toString()));
                        db.updateTask(task);
                        if(selectedTaskId!=0)
                            updateDuration(selectedTaskId);
                        setList(selectedTaskId);

                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alert.create();
        alertDialog.show();

    }

    public void updateDuration(int id){

        TaskDatabaseHandler db = new TaskDatabaseHandler(getBaseContext());

        Task task = db.getTask(id);
        List<Task> subs = db.getTasksFrom(id);
        int totalSubs= 0;
        for(Task t : subs){
            totalSubs += t.getDuration();
        }
        task.setDuration(totalSubs);
        db.updateTask(task);
        if(task.getFrom()!=0)
            updateDuration(task.getFrom());

    }

    @Override
    public void onBackPressed() {
        if(selectedTaskId!=0) {
            //selectedTask
            int back = selectedTask.getFrom();
            setList(back);
        }else finish();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId()==R.id.lvMain) {
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;
            menu.setHeaderTitle(loadedTasks.get(info.position).getTitle());
            String[] menuItems = getResources().getStringArray(R.array.menu);
            for (int i = 0; i<menuItems.length; i++) {
                if(menuItems[i].equals("Finish"))
                {
                    if(loadedTasks.get(info.position).getCheckedBool())
                    {
                        menu.add(Menu.NONE, i, i, "Unfinish");
                    }
                    else menu.add(Menu.NONE, i, i, menuItems[i]);
                }
                else
                    menu.add(Menu.NONE, i, i, menuItems[i]);
            }
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        int menuItemIndex = item.getItemId();
        String[] menuItems = getResources().getStringArray(R.array.menu);
        String menuItemName = menuItems[menuItemIndex];
        String listItemName = loadedTasks.get(info.position).getTitle();

        if(menuItems[menuItemIndex].equals("Add Subtask")){
            Bundle b = new Bundle();
            b.putInt("ID", loadedTasks.get(info.position).getId());
            Intent i = new Intent(this, AddTask.class);
            i.putExtras(b);
            startActivity(i);
        }
        else if(menuItems[menuItemIndex].equals("Delete")){
            deleteTask(loadedTasks.get(info.position).getId(), true);
        }
        else if(menuItems[menuItemIndex].equals("Finish")||menuItems[menuItemIndex].equals("Unfinish")){
            finishTask(loadedTasks.get(info.position).getId(), !loadedTasks.get(info.position).getCheckedBool(), true);
            checkAllSubs(loadedTasks.get(info.position).getFrom());
        }
        else if(menuItems[menuItemIndex].equals("Change Title")){
            changeTitle(loadedTasks.get(info.position).getId());
        }
        else if(menuItems[menuItemIndex].equals("Change Detail")){
            changeDetail(loadedTasks.get(info.position).getId());
        }else if(menuItems[menuItemIndex].equals("Change Duration")){
            changeDuration(loadedTasks.get(info.position).getId());
        }

        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        MenuItem discard = menu.findItem(R.id.action_delete);
        MenuItem finish = menu.findItem(R.id.action_finish);

        if(selectedTaskId==0){
            finish.setVisible(false);
            discard.setVisible(false);
        }else{
            finish.setVisible(true);
            discard.setVisible(true);
            if(selectedTask.getCheckedBool()){
                finish.setIcon(R.drawable.ic_action_cancel);
            }else{
                finish.setIcon(R.drawable.ic_action_accept);
            }
        }



        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id){
            case R.id.action_new:
                Bundle b = new Bundle();
                b.putInt("ID", selectedTaskId);
                Intent i = new Intent(this, AddTask.class);
                i.putExtras(b);
                startActivity(i);
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_delete:
                deleteTask(selectedTaskId, true);
                return true;
            case R.id.action_finish:
                finishTask(selectedTaskId, !selectedTask.getCheckedBool(), true);
                checkAllSubs(selectedTask.getFrom());
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
